# Le jeu du T-Rex


## Etape 1

Bienvenue dans ce TP où vous apprendrez à manipuler Git.  
Vous pouvez effectuer ce TP à l'aide du terminal ou avec une interface graphique comme GitKraken.

Pour commencer `forker le répo sur votre espace Gitlab`

Ensuite vous pouvez ensuite récupérer le projet en local en faisant un `clone` de votre repo :  
Ouvrez votre terminal dans le dossier de votre choix et tapez les commandes suivantes :

```bash
    git clone <url> dino-game
    cd dino-game/
```

Vous aurez besoin de : 
* node, npm : https://nodejs.org/fr/download/
* yarn : `npm install --global yarn`

## Etape 2

Vous avez maintenant tous les outils pour commencer le TP. Nous allons simuler une situation où 3 très bons développeurs ont travaillés en même temps sur le projet de Dinosaure. 

1. Gustave travaille sur l'hébergement (avec Heroku),
2. Armande travaille sur l'intégration de React dans le projet,
3. Dominique est excellent.e avec React et a déjà commencé.e à intégrer le T-Rex depuis Google.

Chacun à fait sa propre branche depuis le premier commit de master (qui contient entre autre ce fichier `readme.md`).  
Elles sont nommées dans le même ordre, comme ceci:

1. `feature/heroku`
2. `feature/react`
3. `feature/dino`

Vous pouvez choisir de changer de branche pour voir l'état d'avancement de chacun, en tapant ligne par ligne (ne faite pas de copier/coller de l'ensemble!) :  
Attention, le readme va changer, vous pourrez revenir dessus en faisant `git checkout master`

```bash
    git checkout feature/heroku
    git checkout feature/react
    git checkout feature/dino
    git checkout master
```

Après chaque ligne tapée, vous pouvez voir l'arboresence de vos fichiers évoluer. Vous ne devez rencontrer aucun problème pour changer de branche. Si c'est le cas c'est que vous avez déjà modifié quelque chose. 

## Etape 3

Afin de tester rapidement notre environnement, nous allons intégrer directement le travail d'Armande. Cela nous permettra de tester React, et de lancer notre site internet en local. Ce sera une première base pour faire nos tests avec Heroku et le Dino.

Nous allons donc merger `feature/react` dans `master`. Mais avant tout nous devons tester le travail d'Armande.

```bash
    git checkout feature/react
    yarn # installe les dépendances
    yarn start # Pour tester le code d'Armande
    ctrl+C # pour arrêter le serveur webpack
```

Maintenant le merge: 

```bash
    git checkout master
    git merge feature/react
```


## Etape 4 

Nous pouvons à présent relancer le serveur webpack pour vérifier que notre site fonctionne toujours après le merge. 
Normalement il n'y a eu des conflits dans le readme, mais il ne devrait pas y avoir de bug.

```bash
    git checkout master # Ceci n'est pas nécessaire si vous êtes déjà sur master
    yarn start # Pour tester le code d'Armande
    ctrl+C # pour arrêter le serveur webpack
```

Continuons en allant voir le travail de Gustave `git checkout feature/heroku`.


## Etape 5

Vous voilà maintenant avec un début de projet sur `master`, vous pouvez dès à présent le tester sur Heroku.
Pour cela vous devez d'abord vous créer un compte sur heroku et suivre la démarche ci-dessous :

1. Créer son compte sur https://id.heroku.com/login
2. Créer une nouvelle app nommée `dinosaur-game-<votre_nom_ou_surnom>`

![créer une app](assets/step-5-1.png)

![nommer son app](assets/step-5-2.png)

3. Aller dans Settings en copier l'url du répo git

![aller dans settings](assets/step-5-3.png)

4. Configurer le buildpack `mars/create-react-app`

![ajout du buildpack](assets/step-5-4.png)

5. Installer la CLI Heroku https://devcenter.heroku.com/articles/heroku-cli


Nous allons maintenant ajouter la remote `heroku` à notre projet. 
Heroku est un service cloud qui permet de lancer des serveurs automatiquement en pushant son code sur les repositories de Heroku. Il s'utilise principalement avec Git et permet de tester facilement ses plateformes, voir de faire des environnements pour chaque développeurs, pour chaque branche et chaque personne qui souhaite tester une application web.


```bash
    git remote add heroku <url>
    heroku login
    git push heroku feature/heroku:master
```

La commande va rater, puisque pour le moment vous n'êtes pas à jour avec le travail d'Armande. 
Pour cela, vous pouvez faire un rebase avec `master` pour déplacer votre branche après le merge fait à l'étape 4.

```bash
    git rebase master
    git push heroku feature/heroku:master
```

Après quelques minutes vous pouvez alors ouvrir votre navigateur à l'URL qui s'affiche dans votre console.

## Etape 6

Retournez dans master et merger `feature/heroku` dans `master`.

## Etape 7 

Basculer sur `feature/dino` pour intégrer le travail de Dominique.
## Etape 8

Tout est prêt pour fonctionner, il ne reste plus qu'à intégrer le code de Dominique. 

* Vous pouvez tout tester en local sur la branche de dominique à l'aide d'un rebase avant de merger
* Vous pouvez merger en local directement sur master et tester après.

Méthode avec le rebase:

```bash
    git checkout feature/dino
    git rebase master
```

Méthode avec le merge:

```bash
    git checkout master
    git merge feature/dino
```

Vous voilà prêt pour tester, vous pouvez faire au choix:

* `git push heroku feature/dino:master` ou `git push heroku master` selon que vous aillez fait un `rebase` ou un `merge` pour tester en ligne.
* `yarn start` pour tester en local.

## Etape 9 

1. Mergez dans `master` si ce n'est pas fait à l'étape 6.
2. Nous allons récupérer un commit qui nous intéresse et qui contient un message pour finir le TP : 

```bash
    git cherry-pick f1b63cc
```

Cette commande permet de récupérer un ou plusieurs commits de n'importe quelle branche et de les appliquer dans l'ordre à votre branche courante.
## Etape 10

Appelez le prof, vous êtes arrivés au bout de votre TP. Félicitations ! 

![gif bravo](https://media.giphy.com/media/MOWPkhRAUbR7i/giphy.gif)

Vous pouvez maintenant pusher votre branche `master` pour sauvegarder votre travail.
