import React from 'react';
import './App.css';
import Dino from './dinosaur/components/Dino';

function App() {
  return (
    <Dino/>
  );
}

export default App;
